package demo.classBasics;

public class Pokemon {
    public static String bouffe = "Pommes";

    public String name;
    public int attack;

    public Pokemon(String name, int attack) {
        this.name = name;
        this.attack = attack;
    }

    public Pokemon battle(Pokemon other) {
        return this.attack > other.attack ? this : other;
    }

    @Override
    public int hashCode() {
        return String.format("%s-%d", name, attack).hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Pokemon)) {
            return false;
        }

        Pokemon obj = (Pokemon) object;

        return obj.name.equals(this.name) && obj.attack == this.attack;
    }
}
