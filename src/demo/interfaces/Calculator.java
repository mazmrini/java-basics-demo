package demo.interfaces;

public class Calculator {
    public double calculate(int a, Operation operation, int b) {
        return operation.calculate(a, b);
    }
}
