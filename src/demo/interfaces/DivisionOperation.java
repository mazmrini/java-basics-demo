package demo.interfaces;

public class DivisionOperation implements Operation {

    @Override
    public double calculate(int a, int b) {
        return (double) a / (double) b;
    }
}
