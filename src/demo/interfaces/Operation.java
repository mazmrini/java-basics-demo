package demo.interfaces;

@FunctionalInterface
public interface Operation {
    double calculate(int a, int b);
}
