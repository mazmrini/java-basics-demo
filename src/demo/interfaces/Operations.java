package demo.interfaces;

public class Operations {
    public static Operation ADD = new AddOperation();
    public static Operation SUBSTRACT = new SubstractOperation();
    public static Operation MULTIPLY = new MultiplyOperation();
    public static Operation DIVIDE = new DivisionOperation();
}
