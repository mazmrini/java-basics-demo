package demo.interfaces;

public class MultiplyOperation implements Operation {

    @Override
    public double calculate(int a, int b) {
        return a * b;
    }
}
