package demo.interfaces;

public class SubstractOperation implements Operation {

    @Override
    public double calculate(int a, int b) {
        return a - b;
    }
}
