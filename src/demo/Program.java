package demo;


import demo.classBasics.Pokemon;
import demo.interfaces.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Program {
    public static void main(String[] args) {
        //demoCalculator();

        demoPokemon();
    }

    private static void demoPokemon() {
        Pokemon charizard = new Pokemon("charizard", 300);
        Pokemon charizard2 = new Pokemon("charizard", 300);
        Pokemon pikachu = new Pokemon("pikachu", 250);

        Map<String, Pokemon> pokemons = new HashMap<>();


        System.out.println(pokemons.get("pika").attack);
    }

    private static void demoCalculator() {
        Calculator calculator = new Calculator();

        System.out.print("5 + 10 = ");
        double result = calculator.calculate(5, new AddOperation(), 10);
        calculator.calculate(5, addOperation(), 10);
        System.out.println(result);

        System.out.print("5 - 10 = ");
        result = calculator.calculate(5, Operations.SUBSTRACT, 10);
        System.out.println(result);

        System.out.print("5 * 10 = ");
        result = calculator.calculate(5, Operations.MULTIPLY, 10);
        System.out.println(result);

        System.out.print("5 / 10 = ");
        result = calculator.calculate(5, Operations.DIVIDE, 10);
        System.out.println(result);
    }

    private static Operation addOperation() {
        return new Operation() {
            @Override
            public double calculate(int a, int b) {
                return a + b;
            }
        };
    }

    private static Operation addOperationv2() {
        return (a, b) -> (double) a + b;
    }
}
